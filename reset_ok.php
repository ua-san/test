<?php

use pq\Connection;

$loop = EvLoop::defaultLoop();
$db   = new Connection('user=postgres application_name=Test', Connection::ASYNC);

$dbIo = $loop->io($db->socket, Ev::READ, function($w) use($db)
{
	static $off = true;

	try
	{
		if($db->poll() === Connection::POLLING_OK)
		{
			if($off)
			{
				$off = false;

				echo "DB - SEND - OK\r\n";
				$db->execAsync("SELECT 'OK'");
			}
			else if($r = $db->getResult())
			{
				echo 'DB - READ - '.$r->fetchRow()[0]."\r\n";
				echo "Pres Enter to restart PostgreSQL server...";
			}
		}
		else
		{
			$off = true;

			$w->stop();

			echo "DB - OFFLINE\r\n";
			echo "Pres Enter to reconnect PostgreSQL client...";
		}
	}
	catch(Throwable $e)
	{
		echo "Error: ".$e->getMessage()."\r\n";
	}
});



$stdIo = $loop->io(STDIN, Ev::READ,	function() use($db, $dbIo)
{
	try
	{
		fgets(STDIN);

		if($db->status === Connection::OK)
		{
			echo "DB - STOPING...\r\n";

			exec('systemctl restart postgresql-9.6');
		}
		else
		{
			$db->resetAsync();

			$dbIo->set($db->socket, $dbIo->events);
			$dbIo->start();

			$db->poll();

			echo "DB - RESET\r\n";
		}
	}
	catch(Throwable $e)
	{
		echo "Error: ".$e->getMessage()."\r\n";
	}
});

$db->poll();
$loop->run();